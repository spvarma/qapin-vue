import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store";

// Landing home page
import HomePage from "../views/landing-home/Index.vue";

// Middleware route

// Auth Login & Registration and Forgot password

import Login from "../components/Login.vue";
import ForgotPassword from "../views/ForgotPassword.vue";
import BasicInfo from "../views/sign-up/BasicInfo";
import AboutProfile from "../views/sign-up/AboutProfile";
import ResetLink from "../views/ResetLink.vue";
import ResetPassword from "../views/ResetPassword.vue";
import PrivacyPolicy from "../views/landing-home/privacy-policy.vue";
import TermsOfService from "../views/landing-home/terms-of-service";
import ContactUs from "../views/landing-home/contact-us";
import About from "../views/landing-home/About.vue";
// View Pages

import Dashboard from "../views/Dashboard.vue";
import NewsFeed from "../views/news-feed/NewsFeed.vue";
import FindJob from "../views/find-job/Index.vue";
import PostJob from "../views/post-job/Index.vue";
import ReviewPost from "../views/review-post/Index.vue";
import UserProfile from "../views/UserProfile.vue";

import Freelancer from "../views/freelancer/Index.vue";
import allContracts from "../views/freelancer/allContracts.vue";
import endContract from "../views/freelancer/endContract.vue";
import jobDetails from "../views/freelancer/jobDetails.vue";
import submitProposal from "../views/freelancer/submitProposal.vue";
import submitProposalslist from "../views/freelancer/submitProposalslist.vue";
import activeProposalslist from "../views/freelancer/activeProposalslist.vue";
import invitationInterviewlist from "../views/freelancer/invitationInterviewlist.vue";
import proposalDetails from "../views/freelancer/proposalDetails.vue";
import myProposals from "../views/freelancer/myProposals.vue";
import Offer from "../views/freelancer/Offer.vue";
import Milestones from "../views/freelancer/Milestones.vue";
import Transaction from "../views/freelancer/Transaction.vue";
import Profile from "../views/freelancer/Profile.vue";
import milestoneSubmition from "../views/freelancer/milestoneSubmition.vue";
import messagesFiles from "../views/freelancer/messagesFiles.vue";
import Settings from "../views/freelancer/Settings.vue";
import Search from "../views/freelancer/Search.vue";
import clientNew from "../views/client/clientNew.vue"; 
import freelanceNew from "../views/freelancer/freelanceNew.vue";
import agencyNew from "../views/freelancer/agencyNew.vue";
import saveJobs from "../views/freelancer/saveJobs.vue";

import postJob from "../views/client/postJob.vue";
import clientDashboard from "../views/client/clientDashboard.vue";
import postReview from "../views/client/postReview.vue";
import jobPosting from "../views/client/jobPosting.vue";
import index from "../views/client/jobpostreview/index.vue";
import jobsDetail from "../views/client/jobsDetail.vue";
import allContract from "../views/client/allContract.vue";
import yourHires from "../views/client/yourHires.vue";
import savedTalent from "../views/client/savedTalent.vue";
import expandTeam from "../views/client/expandTeam.vue";
import Milestone from "../views/client/Milestone.vue";
import milestoneSubmission from "../views/client/milestoneSubmission.vue";
// import inviteDetails from "../views/client/inviteDetails.vue";

// import { ValidationProvider } from 'vee-validate';
Vue.use(VueRouter);
// Vue.component('ValidationProvider', ValidationProvider);

const routes = [
  // default route
  { path: "*", component: clientDashboard },
  {
    path: "/",
    name: "Home Page",
    component: HomePage,
  },

  {
    path: "/client/post-job",
    name: "post-job",
    component: postJob,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/dashboard",
    name: "clientDashboard",
    component: clientDashboard,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/post-review/:pid",
    name: "postReview",
    component: postReview,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/job-posting",
    name: "jobPosting",
    component: jobPosting,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/jobpostreview/index.vue",
    name: "index",
    component: index,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/jobs-detail",
    name: "jobsDetail",
    component: jobsDetail,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/all-contract",
    name: "allContract",
    component: allContract,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/your-hires",
    name: "yourHires",
    component: yourHires,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/saved-talent",
    name: "savedTalent",
    component: savedTalent,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/expand-team",
    name: "expandTeam",
    component: expandTeam,
    meta: { requiresAuth: true },
  },
  {
    path: "/client/milestone",
    name: "Milestone",
    meta: { requiresAuth: true },
    component: Milestone,
  },
  {
    path: "/client/milestone-submission",
    name: "milestoneSubmission",
    meta: { requiresAuth: true },
    component: milestoneSubmission,
  },
  {
    path: "/client/end-contract",
    name: "endContractClient",
    meta: { requiresAuth: true },
    component: endContract,
  },

  // Auth Guard
  {
    path: "/dashboard",
    component: Dashboard,
    meta: { requiresAuth: true },
  },

  
  // Login
  {
    path: "/login",
    name: "Login",
    component: Login,
  },

  // Forgot Password
  {
    path: "/forgot-password",
    name: "ForgotPassword",
    component: ForgotPassword,
    // meta: { requiresAuth: true },
  },
 

  // Expire Link
  {
    path: "/expire-link",
    name: "ExpireLink",
    component: ResetLink,
    // meta: { requiresAuth: true },
  },

  // Reset Link
  {
    path: "/reset-password/:pid",
    name: "ResetPassword",
    component: ResetPassword,
    // meta: { requiresAuth: true },
  },

  // Sign up
  {
    path: "/sign-up",
    name: "BasicInfo",
    component: BasicInfo,
  },

  {
    path: "/sign-up/about-profile",
    name: "AboutProfile",
    component: AboutProfile,
  },

  // View Pages routes

  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
    meta: { requiresAuth: true },
  },
  {
    path: "/news-feed",
    name: "NewsFeed",
    component: NewsFeed,
    meta: { requiresAuth: true },
  },

  {
    path: "/find-job",
    name: "FindJob",
    component: FindJob,
    meta: { requiresAuth: true },
  },

  {
    path: "/post-job",
    name: "PostJob",
    component: PostJob,
    meta: { requiresAuth: true },
  },

  {
    path: "/review-post",
    name: "ReviewPost",
    component: ReviewPost,
    meta: { requiresAuth: true },
  },

  {
    path: "/user-profile",
    name: "UserProfile",
    component: UserProfile,
    meta: { requiresAuth: true },
  },

  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/privacy-policy",
    name: "PrivacyPolicy",
    component: PrivacyPolicy,
    // meta: { requiresAuth: true },
  },
  {
    path: "/terms-of-service",
    name: "TermsOfService",
    component: TermsOfService,
    // meta: { requiresAuth: true },
  },
  {
    path: "/contact-us",
    name: "ContactUs",
    component: ContactUs,
    // meta: { requiresAuth: true },
  },
  {
    path: "/freelancer",
    name: "Freelancer",
    meta: { requiresAuth: true },
    component: Freelancer,
  },
  {
    path: "/agency",
    name: "Agency",
    meta: { requiresAuth: true },
    component: Freelancer,
  },
  {
    path: "/freelancer/job-details",
    name: "jobDetails",
    meta: { requiresAuth: true },
    component: jobDetails,
    props: true
  },
  {
    path: "/freelancer/end-contract",
    name: "endContractFeelancer",
    meta: { requiresAuth: true },
    component: endContract,
  },
  {
    path: "/freelancer/submit-proposal",
    name: "submitProposal",
    meta: { requiresAuth: true },
    component: submitProposal,
  },
  {
    path: "/freelancer/proposal-details",
    name: "proposalDetails",
    meta: { requiresAuth: true },
    component: proposalDetails,
  },
  {
    path: "/freelancer/all-contracts",
    name: "allContracts",
    meta: { requiresAuth: true },
    component: allContracts,
  },
  {
    path: "/freelancer/my-proposals",
    name: "myProposals",
    meta: { requiresAuth: true },
    component: myProposals,
  },
  {
    path: "/freelancer/submit-proposals-list",
    name: "submitProposalslist",
    meta: { requiresAuth: true },
    component: submitProposalslist,
  },
  {
    path: "/freelancer/active-proposals-list",
    name: "activeProposalslist",
    meta: { requiresAuth: true },
    component: activeProposalslist,
  },
  {
    path: "/freelancer/invitation-interview-list",
    name: "invitationInterviewlist",
    meta: { requiresAuth: true },
    component: invitationInterviewlist,
  },
  {
    path: "/freelancer/offer",
    name: "Offer",
    meta: { requiresAuth: true },
    component: Offer,
  },
  {
    path: "/freelancer/milestones",
    name: "Milestones",
    meta: { requiresAuth: true },
    component: Milestones,
  },
  {
    path: "/freelancer/transaction",
    name: "Transaction",
    meta: { requiresAuth: true },
    component: Transaction,
  },
  {
    path: "/freelancer/profile",
    name: "Profile",
    meta: { requiresAuth: true },
    component: Profile,
  },
  {
    path: "/freelancer/milestone-submition",
    name: "milestoneSubmition",
    meta: { requiresAuth: true },
    component: milestoneSubmition,
  },
  {
    path: "/freelancer/messages-files",
    name: "messagesFiles",
    meta: { requiresAuth: true },
    component: messagesFiles,
  },
  {
    path: "/settings",
    name: "Settings",
    meta: { requiresAuth: true },
    component: Settings,
  },
  {
    path: "/freelancer/search",
    name: "Search",
    meta: { requiresAuth: true },
    component: Search,
    // props: true
  },
  {
    path: "/add-freelancer-account",
    name: "addFreelanceAccount",
    meta: { requiresAuth: true },
    component: freelanceNew,
  },
  {
    path: "/add-client-account",
    name: "addFreelanceAccount",
    meta: { requiresAuth: true },
    component: clientNew,
  },
  {
    path: "/add-agency-account",
    name: "agencyNew",
    meta: { requiresAuth: true },
    component: agencyNew,
  },
  {
    path: "/saved-jobs",
    name: "saveJobs",
    meta: { requiresAuth: true },
    component: saveJobs,
  },
]

const router = new VueRouter({
  mode: "history",
  routes,
});

router.beforeEach((to, from, next) => {
  // var accountType = localStorage.getItem("accountType"); 
  // var loadPath = to.path.split("/")[1];
  
  const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);
  const isUserLogin = localStorage.getItem("token");
  store.commit("isNewsfeed", true);
  if (to.name == "NewsFeed" || to.name == "ExpireLink" || to.name=="ResetPassword" || to.name=="Login") {
    store.commit("isNewsfeed", false);
  }
  if (requiresAuth && !isUserLogin) {
    next("/login");
  } else if (requiresAuth && isUserLogin) {
    // console.log(loadPath);
    // if(accountType!=loadPath || loadPath!='add-client-account' || loadPath!= 'settings'){
    //   if(loadPath!='news-feed'){
    //     window.history.back()
    //   }else{
    //     next();
    //   }
    // }else{ 
    //   next();
    // }
    next();
  } else {
    next();
    // if(accountType!=loadPath){
    //   if(loadPath!='news-feed' || loadPath!='add-client-account' || loadPath!= 'settings'){
    //     window.history.back()
    //   }else{
    //     next();
    //   }
    // }else{ 
    //   next();
    // }
  }
});

export default router;
