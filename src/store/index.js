import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import post from "../store/post";
import global from "../store/global";
import settings from "../store/settings";
// stores modules
Vue.use(Vuex);
let baseUrl = "http://qapin_mvp5.manageprojects.in/api/";
export default new Vuex.Store({
  state: {
    isNewsfeed: true,
    lastpostId:0,
    headToken:'',
    listAccount:[],
    accountType:localStorage.getItem("accountType"),
  },

  mutations: {
    isNewsfeed(state, payload) {
      state.isNewsfeed = payload;
    },
    lastpostId(state, payload) {
      state.lastpostId = payload;
    },
    CLEARERROR(state) {
      console.log(state);
    },
    setListOfAccount(state, payload) {
      state.listAccount = payload;
    },

  },

  actions: {
    // async genericAPIGETRequest({ commit, state }, { requestUrl, params }) {
    //   commit('CHANGELOADINGSTATE', { isprocessing: true, processingpercent: 10 });
    //   try {
    //       let res = await Vue.axios.get(baseUrl + requestUrl, { params: params ? params : '' })
    //       if (res.data.response && res.data.responseStatus == 'SUCCESS') {
    //           commit('CLEARERROR');
    //           return res.data.response;
    //       }
    //   } catch (error) {
    //       const nerror = wrapError(error)
    //       commit('SETERROR', nerror)
    //       throw nerror
    //   } finally {
    //       commit('CHANGELOADINGSTATE', { isprocessing: false });
    //   }
    // },

    async genericAPIPOSTRequest({ commit }, { requestUrl, params }) {
      this.headToken = { Authorization: localStorage.getItem("token") };
      try {
        let res;
        if (params) {
          res = await axios.post(baseUrl + requestUrl, params,{"headers":this.headToken});
        } else {
          res = await axios.post(baseUrl + requestUrl);
        }
         
        if(res.data.status){
          commit("CLEARERROR");
          //alert(res.data.message);
          return res.data.result; 
        }else{
          //this.$toastr.e(res.data.message);
          return res.data.result; 
        }
      } catch (error) {
        if(error=='Error: Request failed with status code 401'){
          localStorage.removeItem("token");
          window.location.href = "/login";
        }
        //this.$toastr.e('Something went wrong please try again!');
        // const nerror = wrapError(error)
        // commit('SETERROR', nerror)
        // throw nerror
      } finally {
        //this.$toastr.e('Something went wrong please try again!');
        //commit('CHANGELOADINGSTATE', { isprocessing: false });
      }
    },
    listAccount(context, value) {
      context.commit("setListOfAccount", value);
    }
  },
  
  getters: {
    // getListAccount(state) {
    //   return state.listAccount;
    // }
  },
  modules: {
    post,
    global,
    settings,
  },
});
