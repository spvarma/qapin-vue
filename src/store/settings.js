import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'
let base_url = 'http://qapin_mvp5.manageprojects.in/api/'
let headToken = {
  'Content-Type': 'application/json',
  Authorization: localStorage.getItem('token'),
}
Vue.use(Vuex)

const settings = {
  namespaced: true,

  state: () => ({
    currentUser: null,
  }),

  getters: {
    getUserDetails: (state) => state.currentUser,
  },

  mutations: {
    GET_USER_DETAILS(state, data) {
      state.currentUser = data
    },
    UPDATE_PROFILE(state, data) {
      state.currentUser = data
    },
  },

  actions: {
    fetchUserDetails({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`${base_url}user/getUserDetails`, { headers: headToken })
          .then((response) => {
            commit('GET_USER_DETAILS', response.data.result)
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    updateProfile({ commit }, payload) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${base_url}user/updateProfile`, payload, {
            headers: headToken,
          })
          .then((response) => {
            commit('UPDATE_PROFILE', response.data.result)
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    updateUserConfig({ commit }, payload) {
      console.log(payload)
      return new Promise((resolve, reject) => {
        axios
          .post(`${base_url}setting/updateUserConfig`, payload, {
            headers: headToken,
          })
          .then((response) => {
            commit('UPDATE_PROFILE', response.data.result)
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
  },
}
export default settings
